# Palomar

This project serves as a collection of example projects that demonstrate platform features based on leveraging a microservice architecture. We intend to demonstrate several features ideally with a low impact to application developers.

```mermaid
graph LR;
  Client -- HTTPS --> Ingress;
  Client -. TCP .-> Ingress;
  Ingress{Gateway} -- mTLS --> UI
  UI -- mTLS --> Portfolio
  Portfolio -- mTLS --> Ticker
  Ingress -- mTLS --> Ticker
  Ingress -. TCP .-> Ticker

  style Client fill:#aa2
  style Ingress fill:#0a2
```

## Features

* Consists of a multiple service REST API based application, controlled by policy controls
* Health checks (visualized in Kiali dashboard)
* Gateway terminated TLS
* Mutual TLS (mTLS) between services (service level authentication)
* Service level tracing (emitted via Envoy, visualized in Jaeger)
* Service level monitoring (e.g. load & heath visualization via Grafana)
* Unified deployment strategy
* TCP based services, using proxy from gateway

### Backlog

* Use RBAC to limit AuthorizationPolicies to specific users
* You can deploy non-compliant code without gatekeeper
* API authorization policies via OPA
* Auto-load balancing
* Use Egress rules for external services

### Tracing

The clients must be aware of tracing. There isn't really any magic to get around this. Unless you only allowed a single request into a service behind a sidecar proxy, there is no way to know that an outgoing request is part of a given context/span. There are plenty of support projects to make this accessible for everyone. However, one non-participant (someone who does not forward headers), can break an entire tracing chain.
 
## Notes

* This entire project could be combined into one, but it is being used to show the individual components as separate independent development projects. Make your own decisions on structure based on how development and deployment most natually occurs.
 

Interested in working with us?
Check us out at https://www.chameleoncg.com/#careers
