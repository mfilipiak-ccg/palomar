create-istio-cluster:
	k3d delete ||:
	k3d create --server-arg '--no-deploy=traefik'
	sleep 10
	cp `k3d get-kubeconfig` ~/.kube/config
	istioctl manifest apply -f istio-values.yaml
	kubectl apply -f templates/tls-certs.yaml

deploy-palomar:
	$(eval HTTP_INGRESS_IP=$(shell kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}'))
	test -n "$(HTTP_INGRESS_IP)"
	$(eval HTTP_INGRESS_HOSTNAME="$(HTTP_INGRESS_IP).xip.io")
	@echo "Discovered hostname: $(HTTP_INGRESS_HOSTNAME)"

	kubectl create ns palomar ||:
	kubectl label namespace palomar istio-injection=enabled --overwrite
	# Deny by default in this namespace
	kubectl apply -f templates/default-deny.yaml

	cd palomar-ui && make force-update-k3d HTTP_INGRESS_HOSTNAME="$(HTTP_INGRESS_HOSTNAME)"
	cd palomar-portfolio && make force-update-k3d
	cd palomar-ticker && make force-update-k3d HTTP_INGRESS_HOSTNAME="$(HTTP_INGRESS_HOSTNAME)"
	#kubectl apply -f templates/ui-to-portfolio.yaml

deploy-palomar-standalone:
	kubectl create ns palomar ||:
	cd palomar-ui && make force-update-standalone
	cd palomar-portfolio && make force-update-standalone
	cd palomar-ticker && make force-update-standalone
