# Istio Introduction (v1.5)

Demo video: https://youtu.be/-h9NzJqzWeY

## Setup

These are the setup steps used for the demo

``` sh
# on ubuntu 18.04 (actually Mint 19.2)
> sudo apt-get install -y docker.io git
> curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
> sudo install kubectl -m 755 /usr/local/bin
> curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
> wget -q -O - https://raw.githubusercontent.com/rancher/k3d/master/install.sh |
> curl -L https://istio.io/downloadIstio | sh -

# optional, used for load testing
> wget https://storage.googleapis.com/hey-release/hey_linux_amd64
> sudo install hey_linux_amd64 -m 755 /usr/local/bin/hey
> rm hey_linux_amd64

# optional, used for benchmarking standalone
> https://github.com/txn2/kubefwd/releases/download/1.12.0/kubefwd_amd64.deb
> dpkg -i kubefwd_amd64.deb
> rm kubefwd_amd64.deb

> git clone https://gitlab.com/mfilipiak-ccg/palomar.git
# Make sure the submodules work (sorry, this is setup this way for other devsecops workflow demos)
# Note: you may have to change the ssh -> https in .gitmodules
> git submodule update --init --recursive
```

## Demo script

```sh
palomar> make create-istio-cluster
palomar> make deploy-palomar
palomar> istio dashboard jaegar

# determine IP of LB
palomar> kubectl get svc -n istio-system
# Test 2 URLs that they are unauthorized
palomar> curl https://ui.172.19.0.2.xip.io/user1/portfolio/balance --insecure; echo
palomar> curl https://ticker.172.19.0.2.xip.io/a/ticker --insecure; echo
# Apply the authorization rules for this deployment
palomar> kubectl apply -f templates/ui-to-portfolio.yaml
# Retest URLs to see success (takes a few seconds to propagate)
palomar> curl https://ticker.172.19.0.2.xip.io/a/ticker --insecure; echo
palomar> curl https://ui.172.19.0.2.xip.io/user1/portfolio/balance --insecure; echo
# Check load graphs in integrated visualization tool
palomar> istio dashboard grafana
# Generate some load
palomar> hey https://ui.172.19.0.2.xip.io/user1/portfolio/balance
palomar> telnet ticker.172.19.0.2.xip.io 8888 # send 'a' to it
palomar> 
```

## Standalone benchmark

``` sh
> k3d create
> mkdir -p ~/.kube
> cp $(k3d get-kubeconfig --name='k3s-default') ~/.kube
> make deploy-palomar-standalone
> sudo kubefwd svc -n palomar
```

Run the benchmark. It is not a fair benchmark, it is proxying directly to the UI pod, and not using HTTPS.

``` sh
> hey http://palomar-ui/user1/portfolio/balance
```

In this run, it was roughly 150 requests/sec.
